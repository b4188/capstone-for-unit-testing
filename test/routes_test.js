const chai = require('chai');
const expect = chai.expect;
// same as const {expect} = require("chai")
const http = require('chai-http');

chai.use(http);


describe("API Test Suite", () => {
//1. Check if post /currency is running
  it('Test Post Currency is Running', (done) => {
        chai.request('http://localhost:4001')
        .post('/currency')
        .type('json')
        .send({
    		alias: 'test_post',
    		name: 'test_post',
    		ex: {
    			'peso': 0,
    	        'usd': 0,
    	        'won': 0,
    	        'yuan': 0
    		}
        })
        .end((err, res) => {
        	/*console.log(res.status)*/
        	expect(res.status).to.equal(200);
        	done()
        })
    });

//2. Check if post /currency returns status 400 if name is missing
  	it('Test Post Currency returns status 400 if name is missing', (done) => {
  	      chai.request('http://localhost:4001')
  	      .post('/currency')
  	      .type('json')
  	      .send({
  	  		alias: 'riyadh',
  	  		ex: {
  	  			'peso': 0.47,
  	  	        'usd': 0.0092,
  	  	        'won': 10.93,
  	  	        'yuan': 0.065
  	  		}
  	      })
  	      .end((err, res) => {
  	      	expect(res.status).to.equal(400);
  	      	done()
  	      })
  	  });

//3. Check if post /currency returns status 400 if name is not a string
	it('Test Post Currency returns status 400 if name is not a string', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: 100,
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//4. Check if post /currency returns status 400 if name is empty
	it('Test Post Currency returns status 400 if name is empty', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: '',
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//5. Check if post /currency returns status 400 if ex is missing
	it('Test Post Currency returns status 400 if ex is missing', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: 'Saudi Arabian Riyadh'
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//6. Check if post /currency returns status 400 if ex is not an object
	it('Test Post Currency returns status 400 if ex is not an object', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: 'Saudi Arabian Riyadh',
	  		ex: "Not object but a string"
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//7. Check if post /currency returns status 400 if ex is empty
	it('Test Post Currency returns status 400 if ex is empty', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: 'Saudi Arabian Riyadh',
	  		ex: {}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//8. Check if post /currency returns status 400 if alias is missing
	it('Test Post Currency returns status 400 if alias is missing', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		name: 'Saudi Arabian Riyadh',
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//9. Check if post /currency returns status 400 if alias is not an string
	it('Test Post Currency returns status 400 if alias is not an string', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 100,
	  		name: 'Saudi Arabian Riyadh',
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//10. Check if post /currency returns status 400 if alias is empty
	it('Test Post Currency returns status 400 if alias is empty', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: '',
	  		name: 'Saudi Arabian Riyadh',
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
	it('Test Post Currency returns status 400 if all fields are complete but there is a duplicate alias', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'riyadh',
	  		name: 'Saudi Arabian Riyadh',
	  		ex: {
	  			'peso': 0.47,
	  	        'usd': 0.0092,
	  	        'won': 10.93,
	  	        'yuan': 0.065
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(400);
	      	done()
	      })
	  });

//12. Check if post /currency returns status 200 if all fields are complete and there are no dupicates
	it('Test Post Currency returns status 200 if all fields are complete and there are no dupicates', (done) => {
	      chai.request('http://localhost:4001')
	      .post('/currency')
	      .type('json')
	      .send({
	  		alias: 'test_unique_alias',
	  		name: 'test_unique_name',
	  		ex: {
	  			'peso': 99,
	  	        'usd': 999,
	  	        'won': 9999,
	  	        'yuan': 99999
	  		}
	      })
	      .end((err, res) => {
	      	expect(res.status).to.equal(200);
	      	done()
	      })
	  });
})
