const {exchangeRate} = require('../src/util.js');

//1. Check if post /currency is running
//2. Check if post /currency returns status 400 if name is missing
//3. Check if post /currency returns status 400 if name is not a string
//4. Check if post /currency returns status 400 if name is empty
//5. Check if post /currency returns status 400 if ex is missing
//6. Check if post /currency returns status 400 if ex is not an object
//7. Check if post /currency returns status 400 if ex is empty
//8. Check if post /currency returns status 400 if alias is missing
//9. Check if post /currency returns status 400 if alias is not an string
//10. Check if post /currency returns status 400 if alias is empty
//11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
//12. Check if post /currency returns status 200 if all fields are complete and there are no dupicates

module.exports = (app) => {
    app.post('/currency', (req,res) => {
        //2
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        //3
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        //4
        if(req.body.name.length === 0){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        //5
        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
        }  
        //6
        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error': 'Bad Request - EX must be an object'
            })
        }
        //7
        if(Object.keys(req.body.ex).length === 0){
            return res.status(400).send({
                'error': 'Bad Request - EX must not be empty'
            })
        }   
        //8
        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
        }  
        //9
        if(typeof req.body.alias !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS has to be a string'
            })
        }
        //10
        if(req.body.alias.length === 0){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS must not be empty'
            })
        }  
        //11
        const {alias} = exchangeRate;
        
        if(alias.includes(req.body.alias) === true){
            return res.status(400).send({
                'error': 'Bad Request - ALIAS must be unique'
            })
        }  

        //1 and 12
        return res.status(200).send("POST done");
    })
}
